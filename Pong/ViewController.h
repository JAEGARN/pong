//
//  ViewController.h
//  Pong
//
//  Created by Viktor Jegerås on 2015-02-14.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic) NSTimer *timer;

@property (weak, nonatomic) IBOutlet UITextField *points;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (nonatomic) NSInteger score;
@property (weak, nonatomic) IBOutlet UIImageView *playerPaddle;
@property (weak, nonatomic) IBOutlet UIImageView *ball;


-(void)moveBall;
-(void)startTimer;
-(void)setSizes;
-(void)moveViewWithGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer;

@end

