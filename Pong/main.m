//
//  main.m
//  Pong
//
//  Created by Viktor Jegerås on 2015-02-14.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
