//
//  ViewController.m
//  Pong
//
//  Created by Viktor Jegerås on 2015-02-14.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

bool right = YES;
bool up = NO;
bool isPlaying = YES;
int thePoints;
CGPoint zeBall;
CGFloat speed;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSizes];
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGestureRecognizer:)];
    self.score = 0;
    self.points.text = [NSString stringWithFormat:@"%ld",(long)self.score];

    [self.playerPaddle addGestureRecognizer:panGestureRecognizer];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    
    
}


-(void)moveViewWithGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer{
    CGPoint touchLocation = [panGestureRecognizer locationInView:self.view];
    CGPoint newLoc = CGPointMake(touchLocation.x, self.playerPaddle.center.y);
    
    self.playerPaddle.center = newLoc;
}


-(void)startTimer {
    self.timer = [NSTimer
    scheduledTimerWithTimeInterval:0.05
    target:self
    selector:@selector(moveBall)
    userInfo:nil repeats:YES];

    
}

- (void) stopTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)moveBall{
    
    if(isPlaying){
        
        if (self.ball.center.x > self.view.frame.size.width - self.ball.frame.size.width / 2) {
            right = NO;
        }
        if(self.ball.center.x < self.ball.frame.size.width / 2){
            right = YES;
        }
    
        if (self.ball.center.y > self.view.frame.size.height - self.ball.frame.size.height / 2) {
            isPlaying = NO;
        }
        if(self.ball.center.y < self.ball.frame.size.height / 2){
            up = NO;
            self.points.text = [NSString stringWithFormat:@"%ld",(long)++self.score];
            speed += 0.5f;
        }
    
        if (right) {
            zeBall.x = speed;
        
        }else{
            zeBall.x = -speed;
        }
    
        if (up) {
            zeBall.y = -speed;
        }else{
            zeBall.y = speed;
        }
    
        if (CGRectIntersectsRect(self.playerPaddle.frame, self.ball.frame)) {
            
            if (self.ball.center.x > self.playerPaddle.center.x){
                right = YES;
                zeBall.x = speed;
            }else   {
                right = NO;
                zeBall.x = -speed;
            }
            up = YES;
        }
        
       self.ball.center = CGPointMake(self.ball.center.x + zeBall.x, self.ball.center.y + zeBall.y);
    }else{
        isPlaying = NO;
        self.startButton.hidden = NO;
        self.score = 0;
        [self stopTimer];
    }
}

-(void)setSizes{
    CGSize screenSize = self.view.frame.size;
    CGSize paddleSize = self.playerPaddle.frame.size;
    CGSize ballSize = self.ball.frame.size;
    
    ballSize.width = screenSize.width / 20;
    ballSize.height = screenSize.width / 20;
    
    CGPoint ballPosition = CGPointMake(self.view.center.x, 40);
    
    self.ball.frame = CGRectMake(arc4random_uniform(self.view.frame.size.width-(ballSize.width / 2)), ballPosition.y, ballSize.width, ballSize.height);
    
    paddleSize.width = screenSize.width / 3;
    paddleSize.height = screenSize.height / 30;
    
    CGPoint playerPaddlePosition = CGPointMake(self.view.center.x - paddleSize.width/2, self.view.frame.size.height - paddleSize.height * 3);
    
    self.playerPaddle.frame = CGRectMake(playerPaddlePosition.x, playerPaddlePosition.y, paddleSize.width, paddleSize.height);
    
    self.points.frame = CGRectMake(self.view.center.x - self.points.frame.size.width / 2, self.view.frame.size.height - self.points.frame.size.height, screenSize.width / 5, screenSize.height / 30);
    
    self.startButton.frame = CGRectMake(self.view.center.x - self.startButton.frame.size.width / 2, self.view.center.y, self.view.frame.size.width / 4, self.view.frame.size.height / 30);
    
}
- (IBAction)Start:(id)sender {
    [self setSizes];
    isPlaying = YES;
    speed = 10.0f;
    [self startTimer];
    self.startButton.hidden = YES;
}




@end
